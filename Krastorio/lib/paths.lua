-- -- -- PATHS
-----------------------------------------------------------------------------------------------------------------
-- -- Icons paths
krstr_icon_path                     = "__Krastorio-graphics__/graphics/icons/"
krstr_stacked_icon_path             = "__Krastorio-graphics__/graphics/icons/stacked/"
-- -- Entities icons paths
krstr_entities_icon_path            = "__Krastorio-graphics__/graphics/icons/entities/"
-- -- Equipments icons paths
kr_vehicle_equipment_icons_path     = "__Krastorio-graphics__/graphics/icons/equipment/vehicle/"
kr_universal_equipment_icons_path   = "__Krastorio-graphics__/graphics/icons/equipment/universal/"
kr_character_equipment_icons_path   = "__Krastorio-graphics__/graphics/icons/equipment/character/"
kr_recipes_icon_path                = "__Krastorio-graphics__/graphics/icons/recipes/"
-- -- Equipments sprites paths
kr_vehicle_equipment_sprites_path   = "__Krastorio-graphics__/graphics/equipment/vehicle/"
kr_universal_equipment_sprites_path = "__Krastorio-graphics__/graphics/equipment/universal/"
kr_character_equipment_sprites_path = "__Krastorio-graphics__/graphics/equipment/character/"
-- -- Equipments technologies icons paths
kr_technologies_icons_path          = "__Krastorio-graphics__/graphics/technology/"
-----------------------------------------------------------------------------------------------------------------